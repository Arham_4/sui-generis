# Sui Generis #


An open-source approach to building a unique server with the concept of bringing new ideas to RSPS with source code to the public.
### What is this repository for? ###

* An introduction to new ideas for RSPS.
* An idea for implementation of these new ideas.

### How do I get set up? ###
* ~ shall represent server root.
* Download [the following files](http://puu.sh/kpnml/86d1864d6f.rar).
* Extract to ~/data/.
* Download [the following client](https://mega.nz/#!GYYl1YbJ!zVxhEuZ79bHI4bfzYlhdrwZ4t2rBvBGNaofZ2fovqoI).

### Contributions ###

* The issues system of BitBucket shall be used for submission of new ideas.
* You may fork the repository and add your own ideas.

### Who do I talk to? ###

* For information contact [Arham 4](http://www.rune-server.org/members/arham-4).